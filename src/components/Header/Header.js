import React from "react";

const Header = () => {
  return (
    <header>
      <div className="logo">
        <span>REACT THREE FIBER</span>
      </div>
      <div className="episode">
        <span>EXAMPLE</span>
      </div>
    </header>
  );
};

export default Header;
