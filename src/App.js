import Home from "./components/Home/Home";
import "./App.scss";

const App = () => {
  return <Home />;
};

export default App;
